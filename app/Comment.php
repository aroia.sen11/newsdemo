<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = ['news_id', 'user_id','comment'];

    public function news()
    {
        return $this->hasOne(News::class, 'id','news_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }

}
