<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use App\Comment;
use Auth;
class commentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        try {
            $comment = Comment::findOrFail($id);
            $this->validate($request, [
                'comment' => 'required'
            ]);
            $comment->news_id = $id;
            $comment->user_id = Auth::user()->id;
            $comment->comment = $request->comment;
            $comment->save();
            return back();

        } catch (ModelNotFoundException $e) {
            return redirect('newsController')->with('flash_error', 'Item not found or restricted');
        } catch (\Exception $e) {
            return redirect('newsController')->with('flash_error', 'Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
                $comment = Comment::findOrFail($id);
                $comment->delete();
                return redirect('newsController')->with('flash_success', 'Successfully deleted');

        } catch (ModelNotFoundException $e) {
            return redirect('newsController')->with('flash_error', trans('pos.system.not_found'));
        } catch (\Exception $e) {
            return redirect('newsController')->with('flash_error', trans('Something went wrong'));
        }
    }
}
