<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class newsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allNews = News::latest();
        return view('news.index', compact('allNews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $news = new News;
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required'
        ]);
        $news->title = $request->title;
        $news->text = $request->text;
        $news->save();

        return redirect('newsController')->with('flash_success', 'create news post  successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $news = News::findOrFail($id);
            return view('news.show', compact('news'));

        } catch (ModelNotFoundException $e) {
            return redirect('newsController')->with('flash_error', 'Item not found or restricted');
        } catch (\Exception $e) {
            return redirect('newsController')->with('flash_error', 'Something went wrong');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $news = News::findOrFail($id);
            return view('news.edit', compact('news'));
        } catch (ModelNotFoundException $e) {
            return redirect('newsController')->with('flash_error', 'Item not found or restricted');
        } catch (\Exception $e) {
            return redirect('newsController')->with('flash_error', 'Something went wrong');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $news = News::findOrFail($id);
            $this->validate($request, [
                'title' => 'required',
                'text' => 'required'
            ]);
            $news->title = $request->title;
            $news->text = $request->address;
            $news->save();
            return redirect('newsController')->with('flash_success', 'Modified successfully');

        } catch (ModelNotFoundException $e) {
            return redirect('newsController')->with('flash_error', 'Item not found or restricted');
        } catch (\Exception $e) {
            return redirect('newsController')->with('flash_error', 'Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $news = News::findOrFail($id);
            $comments = $news->comments;
            $newsTitle = $news->title;
            if (count($comments) > 0) {
                return redirect('newsController')->with('flash_error', ' Sorry, you can not store the News because it contains comments');
            } else {
                $news->delete();
                return redirect('newsController')->with('flash_success', 'Successfully deleted' . ' ' . $newsTitle);
            }

        } catch (ModelNotFoundException $e) {
            return redirect('newsController')->with('flash_error', trans('pos.system.not_found'));
        } catch (\Exception $e) {
            return redirect('newsController')->with('flash_error', trans('Something went wrong'));
        }
    }
}
