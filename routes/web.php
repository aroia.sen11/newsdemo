<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['as' => 'newsController.'], function () {
    Route::get('newsController', 'newsController@index')->name('index');
    Route::get('newsController/create', 'newsController@create')->name('create');
    Route::post('newsController', 'newsController@store')->name('store');
    Route::get('newsController/{id}/show', 'newsController@show')->name('show');
    Route::get('newsController/{id}/edit', 'newsController@edit')->name('edit');
    Route::put('newsController/{id}', 'newsController@update')->name('update');
    Route::delete('newsController/{id}', 'newsController@destroy')->name('destroy');
});

Route::group(['as' => 'commentsController.'], function () {
    Route::post('commentsController', 'commentsController@store')->name('store');
    Route::delete('commentsController/{id}', 'commentsController@destroy')->name('destroy');
});
